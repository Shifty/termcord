import threading

import discord

from core.kbhit import KBHit

kb = KBHit()


class Terminal(object):
    def __init__(self, bot):
        self.bot = bot
        self.current_guild = None
        self.current_channel = None
        self.bot.loop.create_task(self.menu())

    async def init_chat(self):
        print('\n')
        messages = await self.current_channel.history(limit=20).flatten()
        for message in reversed(messages):
            self.bot.printer(message)

    async def menu(self):
        self.current_channel = None
        print('1. Guilds\n2. Recent DMs\n3. New DM')
        selected = False
        while not selected:
            choice = self.selection('Select an option: ', 3)
            if int(choice) <= 3:
                if choice == "1":
                    await self.select_guild()
                    selected = True
                elif choice == '2':
                    print('Under development!')
                elif choice == '3':
                    print('Under development!')

    async def select_guild(self):
        guilds = sorted([g for g in self.bot.guilds if g.name], key=lambda g: g.name)
        for index, guild in enumerate(guilds, 1):
            print(f'{index}. {guild.name}')
        choice = self.selection('Select a guild: ', len(self.bot.guilds))
        guild = guilds[int(choice) - 1]
        self.current_guild = guild
        await self.select_channel(guild)

    async def select_channel(self, guild: discord.Guild):
        channels = []
        me = self.current_guild.me
        unsorted = [c for c in guild.channels if isinstance(c, discord.TextChannel)]
        for channel in sorted(unsorted, key=lambda c: c.position):
            if me.permissions_in(channel).read_messages:
                channels.append(channel)
        for index, channel in enumerate(channels, 1):
            print(f'{index}. {channel.name}')
        choice = self.selection('Select a channel: ', len(channels))
        channel = channels[int(choice) - 1]
        self.current_channel = channel
        await self.init_chat()
        self.bot.loop.create_task(self.input_loop())

    @staticmethod
    def selection(text: str, high_range: int):
        selected, out = False, None
        while not selected:
            choice = input(text)
            if choice.isdigit():
                if int(choice) <= high_range:
                    out = choice
                    selected = True
                else:
                    print('Input out of range!')
            else:
                print('Invalid input!')
        return out

    async def handle_message(self, message: str):
        if message == ">menu":
            await self.menu()
        elif message == ">guilds":
            self.current_guild = None
            self.current_channel = None
            await self.select_guild()
        elif message == ">channels":
            self.current_channel = None
            await self.select_channel(self.current_guild)
        elif message == '>refresh':
            await self.init_chat()
            self.bot.loop.create_task(self.input_loop())
        elif message == ">reload":
            print('Reloading.')
            self.bot.reload()
        elif self.current_channel:
            await self.current_channel.send(message)
            self.bot.log(message)

    async def input_loop(self):
        message = ''
        while True:
            if kb.kbhit():
                ch = kb.getch()
                if ch != '\r':
                    message += ch
                else:
                    if message:
                        await self.handle_message(message)
                        message = ''
