import importlib
import json
import sys

import discord

from core.client import Terminal
from core.kbhit import KBHit

kb = KBHit()


class TermCord(discord.Client):
    def __init__(self):
        super().__init__()
        self.terminal = None
        self.log = sys.stdout.write
        with open('core/config.json') as cfg_file:
            cfg = json.load(cfg_file)
        self.token = cfg.get('token')

    def reload(self):
        module = importlib.import_module('core.client')
        importlib.reload(module)
        self.terminal = module.Terminal(self)
        print('Reloaded.')

    def run(self):
        try:
            self.log('Connecting...\n')
            super().run(self.token, bot=True)
        except discord.LoginFailure:
            self.log('Invalid Token!')
            exit()

    async def on_ready(self):
        self.log('Connected\n')
        self.log(f'Logged in as {self.user.name}#{self.user.discriminator}\n')
        self.terminal = Terminal(self)

    def printer(self, message: discord.Message):
        self.log(f'{message.author.display_name}#{message.author.discriminator}: {message.content or "[No Content]"}')

    async def on_message(self, message: discord.Message):
        if self.terminal:
            if self.terminal.current_channel:
                if message.channel.id == self.terminal.current_channel.id:
                    self.printer(message)
